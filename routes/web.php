<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::prefix('view')->group(function () {
	Route::get('/admin/register','AdministratorController@adminView');
	Route::get('/admin/login','SubscribeController@viewlogin');
	Route::get('/customer/register','SubscribeController@customerRegister');
	Route::get('/layout','NewsLetterController@layoutview')->middleware('roles:admin');
});
Route::prefix('admin')->group(function () {
	Route::post('/register','AdministratorController@store');
	Route::post('/login','SubscribeController@loginCustomer');
});

Route::prefix('customer')->group(function () {
	Route::post('/register','SubscribeController@registerCustomer');
});
Route::post('/mail','AdministratorController@index');


Route::get('role',[
   'middleware' => 'roles:admin',
   'uses' => 'AdministratorController@test',
]);