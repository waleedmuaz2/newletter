<?php

namespace App\Http\Controllers;

use App\Administrator;
use Illuminate\Http\Request;
use App\jobs\RegisterUserEmail;
use Illuminate\Validation\Rule;
use Validator;
use App\Http\Helpers\Response;
use App\Http\Requests\UserRegister;
use Illuminate\Support\Facades\Mail;
use App\Subscribe;



class AdministratorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email= Subscribe::get('email');
        foreach ($email as $key) {
                $emailList[]=$key->email;
        }
         RegisterUserEmail::dispatch($emailList)->delay(now()->addSeconds(1));
         return 1;
    }
   
    public function test(){
      echo "<br>Test Controller.";
    }

    /**
     * Show the form for creating a new resource.   
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   
    
    public function adminView(){
        return view('user.admin_register');
    }
    


    /**
     * Display the specified resource.
     *
     * @param  \App\Administrator  $administrator
     * @return \Illuminate\Http\Response
     */
    public function show(Administrator $administrator)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Administrator  $administrator
     * @return \Illuminate\Http\Response
     */
    public function edit(Administrator $administrator)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Administrator  $administrator
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Administrator $administrator)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Administrator  $administrator
     * @return \Illuminate\Http\Response
     */
    public function destroy(Administrator $administrator)
    {
        //
    }
    public function store(UserRegister $request){

           $admin = new Administrator;
           $admin->name=$request->name;
           $admin->email=$request->email;
           $admin->password=$request->password;
           $admin->role="admin";
           $admin->save();
           return 1;
    }
    
}
