<?php

namespace App\Http\Controllers;

use App\Subscribe;
use Illuminate\Http\Request;
use App\Http\Requests\SubcribeValid;
use App\Administrator;




class SubscribeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subscribe  $subscribe
     * @return \Illuminate\Http\Response
     */
    public function show(Subscribe $subscribe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subscribe  $subscribe
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscribe $subscribe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subscribe  $subscribe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subscribe $subscribe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subscribe  $subscribe
     * @return \Illuminate\Http\Response
     */
   public function loginCustomer(Request $request){
            $login= Administrator::where('email',$request->email)->where('password',$request->password)->get();
            if(isset($login)==0){
                 return redirect('/view/admin/login');
            }
            else{
                //true
                session()->put('email',$request->email);
                // Session::get('email')=$request->email;
                return redirect('/view/layout');
            }
    }
    public function viewlogin()
    {
        return view('user.admin_login');
    }
    


    public function registerCustomer(SubcribeValid $request){
           $admin = new Subscribe;
           $admin->email=$request->email;
           $admin->save();
           return 1;
    }
    public function customerRegister(){
        return view('user.customer_register');
    }

}
