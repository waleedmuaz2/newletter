<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class Roles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$roles)
    {
        $email=session()->get('email');
        $users = DB::table('administrators')->where('email',$email)->get('role');
        if($users[0]->role=='admin'){
            echo $roles;
        }
        else{
            return redirect()->back();
        }

        return $next($request);
    }
}
