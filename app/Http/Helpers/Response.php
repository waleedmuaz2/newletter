<?php

namespace App\Http\Helpers;

class Response
{
    public static function success_response($message, $data = '')
    {
        $result = [];

        $result['success'] = true;
        $result['message'] = $message;

        if ($data){
            $result['data'] = $data;
        }

        http_response_code(200);
        header('Content-Type: application/json');
        echo json_encode($result);
        exit;   
    }

    public static function error_response($message, $code = 200, $debug = '', $additional = '')
    {
        $result = [];

        if(is_array($message)){
            $message = self::validation_errors_format($message);
        }

        if ($additional){
            $result = $additional;
        }
        $result['success'] = false;
        $result['message'] = $message;

        if(!empty($debug)){
            $result['debug'] = $debug;
        }

        http_response_code($code);
        header('Content-Type: application/json');
        echo json_encode($result);
        exit;
    }

    public static function validation_errors_format($errors)
    {
        return implode('', $errors);
    }
}
