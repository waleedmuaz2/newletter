<!DOCTYPE html>
<html>
<head>
  <title>Al-Wasaq | Admin</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
   <div class="row">
    
    <header>
         <div class="col-sm-12">
          <h1>Press Submit To send Email</h1>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
        </div>
    </header>
    
    <aside>
      <div class="col-sm-3">

      </div>
      <div class="col-sm-6">
        <div>
          <center>
          <a href="#">
          </a>
        </center>
      </div>
          <form id="registerForm">
        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
             



              <div class="form-group">
                <input type="submit" id="hid" class="btn-block btn btn-success" style="height: 100px">
              </div>

          </form> 
              <span  style="color: red;list-style: none;" id="errors"></span>
              <span  style="color: #6d7a69;list-style: none" id="success"></span>
      </div>
      <div class="col-sm-3">
      </div>
    </aside>
  </div>
</div>
<script type="text/javascript">
 $('#registerForm').submit(function(e){
  $("#hid").css("display", true);
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: '/mail', 
        data: $(this).serialize(),
        dataType: 'json',
        error: function(data){
          $('#success').empty(); 
          $('#errors').empty(); 
          var x=JSON.parse(data.responseText);
          for (var error in x.errors) {
            $('#errors').append("<span class='alert alert-danger btn-block' ><li>"+x.errors[error]+"</li></span>");
          }
            $("#hid").css("disabled", false);
        },
        success: function(data){
          $('#success').empty(); 
          $('#errors').empty(); 
            $('#success').append("<span class='alert alert-success btn-block' ><li>Successfully</li></span>");
            $("#hid").attr("disabled", false);
        }
    });
});
</script>

</body>
</html>