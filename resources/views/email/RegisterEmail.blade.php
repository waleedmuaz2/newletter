@component('mail::message')
# Welcome NewsLetter Al-Wasaq Task


@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
