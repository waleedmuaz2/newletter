<!DOCTYPE html>
<html>
<head>
  <title>Al-Wasaq | Admin</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
   <div class="row">
    
    <header>
         <div class="col-sm-12">
          <h1>Admin Login</h1>
        </div>
    </header>
    
    <aside>
      <div class="col-sm-3">

      </div>
      <div class="col-sm-6">
        <div>
          <center>
          <a href="#">
            <img src="https://cdn.tutsplus.com/net/uploads/legacy/2064_laravel/images/main_image.png" height="30%" width="30%" style="border-radius: 50% 50%">
          </a>
        </center>
      </div>
          <form method="post" action="{{url('/admin/login')}}">
        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
              <div class="form-group">
                <label for="Email">Email:</label>
                <input type="text" class="form-control" placeholder="Enter Email..." id="Email" name="email">
              </div>
              <div class="form-group">
                <label for="Email">Password:</label>
                <input type="Password" class="form-control" placeholder="Enter Password..." id="Password" name="password">
              </div>
              <div class="form-group">
                <input type="submit" id="hid" class="btn-block btn" placeholder="Enter Re-Password...">
              </div>
          </form> 

      </div>
      <div class="col-sm-3">
      </div>
    </aside>
  </div>
</div>


</body>
</html>