<!DOCTYPE html>
<html>
<head>
  <title>Al-Wasaq | Admin</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
   <div class="row">
    
    <header>
         <div class="col-sm-12">
          <h1>Admin Register</h1>
        </div>
    </header>
    
    <aside>
      <div class="col-sm-3">

      </div>
      <div class="col-sm-6">
        <div>
          <center>
          <a href="#">
            <img src="https://cdn.tutsplus.com/net/uploads/legacy/2064_laravel/images/main_image.png" height="30%" width="30%" style="border-radius: 50% 50%">
          </a>
        </center>
      </div>
          <form id="registerForm">
        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
              <span  style="color: red;list-style: none" id="errors"></span>
              <span  style="color: #6d7a69;list-style: none" id="success"></span>
              <div class="form-group">
                <label for="Name">Name:</label>
                <input type="text" class="form-control" placeholder="Enter Name..." id="Name" name="name">
              </div>
              <div class="form-group">
                <label for="Email">Email:</label>
                <input type="text" class="form-control" placeholder="Enter Email..." id="Email" name="email">
              </div>
              <div class="form-group">
                <label for="Email">Password:</label>
                <input type="Password" class="form-control" placeholder="Enter Password..." id="Password" name="password">
              </div>
              <div class="form-group">
                <label for="Email">Re-Password:</label>
                <input type="Password" class="form-control" placeholder="Enter Re-Password..." id="Re-Password" name="repassword">
              </div>
              <div class="form-group">
                <input type="submit" id="hid" class="btn-block btn" placeholder="Enter Re-Password...">
              </div>
          </form> 
              <a class="btn btn-block btn-primary stretched-link" href="{{url('view/admin/login')}}">Login</a>
      </div>
      <div class="col-sm-3">
      </div>
    </aside>
  </div>
</div>
<script type="text/javascript">
 $('#registerForm').submit(function(e){
  $("#hid").css("display", true);
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: '/admin/register', 
        data: $(this).serialize(),
        dataType: 'json',
        error: function(data){
          $('#success').empty(); 
          $('#errors').empty(); 
          var x=JSON.parse(data.responseText);
          for (var error in x.errors) {
            $('#errors').append("<span class='alert alert-danger btn-block'><li>"+x.errors[error]+"</li></span>");
          }
            $("#hid").css("disabled", false);
        },
        success: function(data){
          $('#success').empty(); 
          $('#errors').empty(); 
            $('#success').append("<span class='alert alert-success btn-block' ><li>Register Successfully</li></span>");
            $('#Name').val(null);
            $('#Email').val(null);
            $('#Password').val(null);
            $('#Re-Password').val(null);
            $("#hid").attr("disabled", false);
        }
    });
});
</script>

</body>
</html>